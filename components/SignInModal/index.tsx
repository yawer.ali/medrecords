import React, { useState } from "react";
import { FcGoogle } from "react-icons/fc";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
interface FormInputs {
  email: string;
  password: string;
}
interface props {
  show: (setShowModal: boolean) => void;
  //   showSignUp: (setShowSecondModal: boolean) => void;
}

const schema = yup.object().shape({
  email: yup.string().email().required("Email is a required field"),
  password: yup
    .string()
    .required("Please enter your password")
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      // "Password must contain at least 8 characters, one uppercase, one number and one special case character"
      "Password must contain at least 8 characters"
    ),
});

const SignIn = (props: props) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormInputs>({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: any) => {
    // console.log(data);
    axios
      .post("https://sql-dev-india.thewitslab.com:3003/auth/login", data)
      .then((response) => {
        localStorage.setItem("token", response.data.token);
        let token = localStorage.getItem("token");
        // console.log(token);
        // token ? navigate("/") : navigate("/loginAndsignup");
      });
  };
  return (
    <LoginMainWrapper>
      <LoginWrapper>
        <BackButton onClick={() => props.show(false)}>X</BackButton>
        <LoginHeadingWrapper>
          <LoginHeading>Sign in</LoginHeading>
        </LoginHeadingWrapper>
        <LoginForm onSubmit={handleSubmit(onSubmit)}>
          <LabelWrapper>
            <Label>Email</Label>
          </LabelWrapper>
          <InputWrapper>
            <Input type="Email" {...register("email")} />
          </InputWrapper>
          {errors.email && <ErrorMsg>{errors.email?.message}</ErrorMsg>}
          <LabelWrapper>
            <Label>Password</Label>
          </LabelWrapper>
          <InputWrapper>
            <Input type="Password" {...register("password")} />
          </InputWrapper>
          {errors.password && <ErrorMsg>{errors.password?.message}</ErrorMsg>}
          <LoginButtonWrapper>
            <LoginButton type="submit">Login</LoginButton>
          </LoginButtonWrapper>
          <div>
            <div style={{ textAlign: "center" }}>Or</div>
            <LoginGoogle type="submit">
              <FcGoogle className="google" />
              {/* Login With Google */}
            </LoginGoogle>
          </div>
        </LoginForm>
        {/* <NotHaveAccWrapper>
          <Text>Don't have an account? </Text>
          <TextLink onClick={() => props.showSignUp(true)}> Signup</TextLink>
        </NotHaveAccWrapper> */}
      </LoginWrapper>
    </LoginMainWrapper>
  );
};

export default SignIn;

const LoginMainWrapper = styled.div`
  position: absolute;
  top: 150%;
  right: 42%;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const LoginWrapper = styled.div`
  background-color: #e7e9eb;
  padding: 4vw 2vw;
  border-radius: 1vw;
`;
const LoginHeadingWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const LoginHeading = styled.h2`
  color: black;
`;

const LoginForm = styled.form``;
const LabelWrapper = styled.div`
  padding: 0.2vw 0.2vw;
`;
const Label = styled.label`
  font-size: 1vw;
  color: black;
  font-weight: 500;
`;
const InputWrapper = styled.div`
  padding: 0.2vw 0.2vw;
`;
const Input = styled.input`
  width: 16vw;
  height: 3.5vh;
  border: none;
  border-radius: 8px;
  background-color: white;
`;

const LoginButtonWrapper = styled.div`
  padding: 1vw 0;
  width: 16.3vw;
  margin-left: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const LoginButton = styled.button`
  width: 16.3vw;
  padding: 0.6vw;
  background-color: #2e85f7;
  color: white;
  font-size: 1vw;
  font-weight: 900;
  border: none;
  border-radius: 8px;
`;
const LoginGoogle = styled.button`
  width: 16.3vw;
  padding: 0.6vw;
  background-color: white;
  color: black;
  border: none;
  border-radius: 8px;
  .google {
    font-size: 2vw;
  }
`;

// const NotHaveAccWrapper = styled.div`
//   display: flex;
//   align-items: center;
//   justify-content: center;
// `;
// const Text = styled.span`
//   font-size: 1vw;
//   color: black;
//   font-weight: 500;
// `;
// const TextLink = styled.span`
//   font-size: 1vw;
//   color: #1877f2;
//   cursor: pointer;
// `;

const BackButton = styled.button`
  background-color: #2e85f7;
  border-radius: 5%;
  border: none;
  color: white;
  font-size: 0.8vw;
  font-weight: 500;
  :hover {
    cursor: pointer;
    background-color: red;
    color: black;
    transform: scale(1.2);
  }
`;

const ErrorMsg = styled.span`
  color: red;
  font-size: 0.7vw;
  margin-left: 10px;
`;
