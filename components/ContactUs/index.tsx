import React from "react";
import styled from "styled-components";

export const ContactUs = () => {
  return (
    <ContactUsWrapper>
      <Heading>Contact Us</Heading>
      <EmailWrapper>
        <EmailInput placeholder="Email" />
      </EmailWrapper>
      <MsgWrapper>
        <MsgText placeholder="Your Message" />
      </MsgWrapper>
      <ButtonWrapper>
        <SubmitButton>Submit</SubmitButton>
      </ButtonWrapper>
    </ContactUsWrapper>
  );
};

//styles
const ContactUsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 16px;
`;
const Heading = styled.div`
  font-size: 60px;
`;
const EmailWrapper = styled.div`
  color: rgb(255, 255, 255);
  width: 100%;
  max-width: 500px;
`;
const EmailInput = styled.input`
  width: 100%;
  background: rgba(255, 255, 255, 0.1);
  border: 1px solid rgba(0, 0, 0, 0.2);
  border-radius: 4px;
  font-size: 18px;
  padding: 20px;
`;

const MsgWrapper = styled.div`
  width: 100%;
  max-width: 500px;
`;
const MsgText = styled.textarea`
  background: rgba(255, 255, 255, 0.1);
  border: 1px solid rgba(0, 0, 0, 0.2);
  border-radius: 4px;
  width: 100%;
  height: 144px;
  color: rgb(255, 255, 255);
  font-family: OutfitRegular;
  font-style: normal;
  font-size: 18px;
  line-height: 18px;
  outline: none;
  padding: 15px 20px;
`;
const ButtonWrapper = styled.div`
  width: 100%;
  max-width: 500px;
`;
const SubmitButton = styled.button`
  width: 100%;
  cursor: pointer;
  padding: 0px 7px;
  background: rgb(68, 115, 245);
  color: rgb(255, 255, 255);
  border: none;
  font-size: 14px;
  font-family: OutfitSemiBold;
  height: 50px;
  border-radius: 16px;
`;
