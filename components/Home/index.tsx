import Image from "next/image";
import React from "react";
import styled from "styled-components";
import HospitalImg from "../../public/assets/hospital.png";

export const HomeCom = () => {
  return (
    <HomeWrapper>
      <TextAndButtonWrapper>
        <HeadingWrapper>
          Easy. Secure.
          <br /> MedRecords <br />
          Planning.
        </HeadingWrapper>
        <TextWrapper>
          Your records also have the results of medical tests, treatments,
          medicines, and any notes doctors make about you and your health.
          Medical records aren't only about your physical health. They also
        </TextWrapper>
        <ButtonWrapper>
          <Button>Get Started</Button>
        </ButtonWrapper>
      </TextAndButtonWrapper>
      <ImageWrapper>
        <Image src={HospitalImg} alt="img" className="Img" />
      </ImageWrapper>
    </HomeWrapper>
  );
};

//styles

const HomeWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 100px;
  padding: 80px 10px;
`;
const TextAndButtonWrapper = styled.div``;
const HeadingWrapper = styled.div`
  font-size: 53px;
`;
const TextWrapper = styled.div`
  font-family: OutfitRegular;
  width: 100%;
  max-width: 750px;
  font-size: 20px;
  line-height: 29px;
  color: rgb(229, 229, 229);
  padding: 30px 0;
`;
const ButtonWrapper = styled.div``;
const Button = styled.button`
  width: 100%;
  max-width: 236px;
  cursor: pointer;
  padding: 0px 7px;
  background: rgb(68, 115, 245);
  color: rgb(255, 255, 255);
  border: none;
  font-size: 14px;
  font-family: OutfitSemiBold;
  height: 50px;
  border-radius: 16px;
  font-size: 18px;
  height: 53px;
  background: rgb(14, 101, 243);
`;
const ImageWrapper = styled.div`
  /* border: 2px solid red; */
  margin-top: 60px;
  .Img {
    width: 500px;
    height: 400px;
    border: none;
  }
`;
